## Ejercicio - Encontrando marca en un grupo

Define el método `brand_group` que recibirá un nombre de marca `string` como argumento y retornará otro string que corresponde al grupo de marcas al que pertenece, este string retornado es la clave `key` de un hash `brand_groups` que contiene todos los grupos de marcas. 

Todas las pruebas en el `driver code` deben ser true.


```ruby
#brand groups
BRAND_GROUPS = {
   "celular" => ['iphone','lg', 'samsung', 'motorola'], 
   "computadora" => ['MacBook', 'HP', 'Toshiba', 'Dell'], 
   "cerveza" => ['Stella', 'Bohemia', 'Heineken', 'Corona'], 
   "refresco" => ['coca-cola', 'Yoli', 'Sprite', 'Fanta']
   }


#brand_group method





#Driver code

p brand_group('Yoli') == "refresco"
p brand_group('Heineken') == "cerveza"
p brand_group('HP') == "computadora"
p brand_group('Tecate') == "Marca no encontrada"
```
